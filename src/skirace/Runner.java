//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

public class Runner {
    private final String id; //DDMMYYNNNNN ; DD-MM-YY of adding runner, NNNNN - record number which runner registered in that day
    private String name;
    private String surname;
    private String birthYear;
    private String category;
    private String club;

    public Runner(String id){
        this.id = id;
        //constructor only for database reader clarity
    }
    public Runner(String id, String name, String surname, String birthYear) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
        this.category = "open";
        this.club = "unaffiliated";
    }

    public Runner(String id, String name, String surname, String birthYear, String category, String club) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
        if (category.isEmpty()){
            category = "open";
        }
        this.category = category;
        if (club.isEmpty()){
            club = "unaffiliated";
        }
        this.club = club;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    @Override
    public String toString() {
        return "Runner{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthYear='" + birthYear + '\'' +
                ", category='" + category + '\'' +
                ", club='" + club + '\'' +
                '}';
    }
}
