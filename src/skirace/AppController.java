//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class AppController {
    private final JPanel cardsPanel;
    private final CardLayout cards;
    private ArrayList<Runner> runners = new ArrayList<>();
    private ArrayList<Race> races = new ArrayList<>();
    private HashMap<Runner, Integer> nos = new HashMap<>();
    private HashMap<Runner, Integer> order = new HashMap<>();
    private HashMap<Runner, LocalTime> times1 = new HashMap<>();
    private HashMap<Runner, LocalTime> times2 = new HashMap<>();
    private HashMap<Runner, Duration> timesDiff = new HashMap<>();
    private HashMap<Runner, Integer> code = new HashMap<>();
    private HashMap<Runner, Integer> scores = new HashMap<>();

    private final ArrayList<Panel> panels;
    private Panel currentPanel;
    private int currentIndex = 0;



    public AppController(){
        JFrame mainFrame = new JFrame("Ski Race");
        JLabel contentPane = new JLabel();


        cards = new CardLayout();

        cardsPanel = new JPanel(cards);
        mainFrame.add(cardsPanel, BorderLayout.CENTER);

        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("src/skirace/SkiRaceIcon.png"));
        } catch (IOException ignored) {
        }
        mainFrame.setIconImage(img);
        mainFrame.setSize(850, 450);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setResizable(false);
        mainFrame.setLocationRelativeTo(null);

        JButton[] prevButtons = new JButton[3];
        JButton[] nextButtons = new JButton[3];
        for(int i = 0; i < 3; i++) {
            JButton bPrev = new JButton("Prev");
            JButton bNext = new JButton("Next");
            bPrev.setBackground(Color.magenta);
            bNext.setBackground(Color.magenta);
            bPrev.setFocusable(false);
            bNext.setFocusable(false);
            bPrev.addActionListener(e -> switchPanels(-1));
            bNext.addActionListener(e -> switchPanels(1));
            nextButtons[i] = bNext;
            prevButtons[i] = bPrev;
        }

        WelcomePanel welcomePanel = new WelcomePanel(nextButtons[0]);
        RegisterPanel registerPanel = new RegisterPanel(prevButtons[0], nextButtons[1]);
        RunningPanel runningPanel = new RunningPanel(prevButtons[1], nextButtons[2]);
        ScorePanel scorePanel = new ScorePanel(prevButtons[2]);
        panels = new ArrayList<>();
        Collections.addAll(panels, welcomePanel, registerPanel, runningPanel, scorePanel);

        cardsPanel.add(welcomePanel.panel, "wp");
        cardsPanel.add(registerPanel.panel, "rp");
        cardsPanel.add(runningPanel.panel, "rup");
        cardsPanel.add(scorePanel.panel, "sp");
        cards.first(cardsPanel);

        currentPanel = welcomePanel;

        cardsPanel.setBackground(Color.yellow);

        mainFrame.getContentPane().setBackground(Color.green);
        mainFrame.pack();

        mainFrame.setVisible(true);
    }

    private void switchPanels(int direction){
        getData();

        currentIndex += direction;
        currentPanel = panels.get(currentIndex);
        if(direction == 1){
            cards.next(cardsPanel);
        } else {
            cards.previous(cardsPanel);
        }

        currentPanel.updateData(races, runners, nos, order, times1, times2, timesDiff, code, scores);
        currentPanel.updateTable();
        //index of running panel
        int RUNNING_INDEX = 2;
        if (currentIndex == RUNNING_INDEX){
            currentPanel.refresh();
        }
    }

    private void getData(){
        races = currentPanel.getRaces();
        runners = currentPanel.getRunners();
        nos = currentPanel.getNos();
        order = currentPanel.getOrder();
        times1 = currentPanel.getTimes1();
        times2 = currentPanel.getTimes2();
        timesDiff = currentPanel.getTimesDiff();
        scores = currentPanel.getScores();
        code = currentPanel.getCode();
    }
}


