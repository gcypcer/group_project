//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Panel for phase 2
 */
public class RunningPanel extends Panel {
    private final JTable table;
    private final DefaultTableModel model;
    private int startIndex = 0;
    private int finishIndex = 0;


    public JButton bCalculate;

    public RunningPanel(JButton bPrev, JButton bNext){
        this.bPrev = bPrev;
        this.bNext = bNext;

        Image backgroundImage = new ImageIcon("resources/running_background.png").getImage();
        image = backgroundImage;

        bCalculate = new JButton("Calculate results");

        // North
        JPanel pNorth = new JPanel(new GridLayout(1, 2, 10, 0));
        pNorth.setBorder(new EmptyBorder(10, 10, 0, 10));

        JLabel clock = new JLabel();
        clock.setFont(new Font("Serif", Font.BOLD, 22));
        ActionListener updateClockAction = e -> clock.setText(LocalTime.now().truncatedTo(ChronoUnit.MILLIS).toString());
        Timer t = new Timer(1000, updateClockAction);
        t.start();

        JButton bStart = new JButton("Manual start");
        bStart.setBackground(Color.green);
        bStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                while(startIndex < table.getRowCount() && (code.get(runners.get(startIndex)) != READY_CODE)){ //lets find the next ready runner
                    if(times1.get(runners.get(startIndex)) == null){
                        times1.put(runners.get(startIndex),LocalTime.now());
                    }
                    startIndex++;
                }
                if(startIndex < table.getRowCount()) {
                    LocalTime start = LocalTime.now();
                    code.put(runners.get(startIndex),RUNNING_CODE);
                    times1.put(runners.get(startIndex++), start);
                    updateTable();
                }

                bStart.setEnabled(false);
                int delay = races.get(0).STARTING_INTERVAL*1000;
                Timer timer = new Timer(delay,evt->bStart.setEnabled(true));
                timer.start();

            }
        });
        JButton bStop = new JButton("Manual stop");
        bStop.setBackground(Color.red);
        bStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                while(finishIndex < table.getRowCount() && finishIndex < startIndex && (code.get(runners.get(finishIndex)) != RUNNING_CODE)){
                    if(times2.get(runners.get(finishIndex)) == null){
                        times2.put(runners.get(finishIndex),LocalTime.now());
                    }
                    finishIndex++;
                }
                if(finishIndex < table.getRowCount() && finishIndex < startIndex) {
                    if(finishIndex == runners.size()-1){
                        bCalculate.setEnabled(true);
                    }
                    LocalTime finish = LocalTime.now();
                    Duration diff = Duration.between(times1.get(runners.get(finishIndex)),finish);
                    if(diff.getSeconds() < races.get(0).MIN_RESULT || diff.getSeconds() > races.get(0).MAX_RESULT){
                        timesDiff.put(runners.get(finishIndex), Duration.between(LocalTime.now(),LocalTime.now()));
                        code.put(runners.get(finishIndex), DSQ_CODE);
                        times2.put(runners.get(finishIndex++), finish);
                    }
                    else {
                        timesDiff.put(runners.get(finishIndex), diff);
                        code.put(runners.get(finishIndex), FINISHED_CODE);
                        times2.put(runners.get(finishIndex++), finish);
                    }
                    updateTable();
                }
                refresh();
            }
        });
        JPanel pStartStop = new JPanel();
        pStartStop.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 0));
        pStartStop.add(bStart);
        pStartStop.add(bStop);
        JPanel pClock = new JPanel();
        pClock.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
        pClock.add(clock);
        pNorth.add(pClock);
        pStartStop.setOpaque(false);
        pNorth.add(pStartStop);
        pNorth.setOpaque(false);
        panel.add(pNorth, BorderLayout.NORTH);

        // South
        JPanel pSouth = new JPanel(new GridLayout(1, 0, 0, 0));
        pSouth.setBorder(new EmptyBorder(0, 10, 10, 10));
        JPanel pPrevNext = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
        JPanel pCalculate = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        bCalculate.addActionListener(e -> {
            scores = new HashMap<>();
            ArrayList<Duration> results = new ArrayList<>();
            for(Runner r : runners){
                if(code.get(r) != FINISHED_CODE){
                    results.add(null);
                }
                else {
                    results.add(Duration.between(times1.get(r), times2.get(r)));
                }
            }
            ArrayList<Duration> sortedResults = new ArrayList<>(results);
            sortedResults.sort(Comparator.nullsLast(Comparator.naturalOrder()));
            for(int i = 0; i < runners.size(); i++){
                if(code.get(runners.get(i)) == FINISHED_CODE && !runners.get(i).getCategory().equals("forlaufer")) {
                    switch (races.get(0).SCORE_ALGORITHM_TYPE){
                        case 1:
                            scores.put(runners.get(i), runners.size() - sortedResults.indexOf(results.get(i)));
                            break;
                        case 2:
                            scores.put(runners.get(i), runners.size()/(sortedResults.indexOf(results.get(i))+1));
                            break;
                        case 3:
                            scores.put(runners.get(i), max(runners.size()/(2<<min(sortedResults.indexOf(results.get(i)),10)),1));
                            break;
                        default:
                            scores.put(runners.get(i), sortedResults.indexOf(results.get(i))+1);
                            break;
                    }
                }
                else{
                    scores.put(runners.get(i), 0);
                }
            }
            updateTable();
            roundNumber++;
            DataBaseControl db = new DataBaseControl();
            races.get(0).addRunners(runners);
            races.get(0).setTimes1(times1);
            races.get(0).setTimes2(times2);
            races.get(0).setScores(scores);
            races.get(0).setCodes(code);
            db.writeRaces(races);

            startIndex = 0;
            finishIndex = 0;
        });
        bCalculate.setEnabled(false);
        pCalculate.add(bCalculate);
        pCalculate.setOpaque(false);
        pSouth.add(pCalculate);

        pPrevNext.add(this.bPrev);
        pPrevNext.add(this.bNext);
        pPrevNext.setOpaque(false);

        pSouth.add(pPrevNext);

        pSouth.setOpaque(false);

        panel.add(pSouth, BorderLayout.SOUTH);

        // Center
        JPanel pCenter = new JPanel();
        model = new DefaultTableModel();
        table = new JTable(model);

        String[] columnNames = {"ID.", "Runner no.", "Name", "Surname", "Time1", "Time2", "TimeDiff", DNS_STRING , DNF_STRING, DSQ_STRING,"code"};
        for(String col : columnNames){
            model.addColumn(col);
        }

        updateTable();

        table.getColumn(DNS_STRING).setCellRenderer(new ButtonRenderer());
        table.getColumn(DNS_STRING).setCellEditor(new ButtonEditor(new JCheckBox()));

        table.getColumn(DNF_STRING).setCellRenderer(new ButtonRenderer());
        table.getColumn(DNF_STRING).setCellEditor(new ButtonEditor(new JCheckBox()));

        table.getColumn(DSQ_STRING).setCellRenderer(new ButtonRenderer());
        table.getColumn(DSQ_STRING).setCellEditor(new ButtonEditor(new JCheckBox()));


        table.setPreferredScrollableViewportSize(new Dimension(800, 210));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        pCenter.add(scrollPane);
        pCenter.setOpaque(false);

        panel.add(pCenter, BorderLayout.CENTER);
    }

    @Override
    public void updateTable() {
        model.setRowCount(0);
        for(Runner r : runners){
            LocalTime t1 = times1.get(r);
            if(t1 != null) {t1 = t1.truncatedTo(ChronoUnit.MILLIS);}
            LocalTime t2 = times2.get(r);
            if(t2 != null) {t2 = t2.truncatedTo(ChronoUnit.MILLIS);}
            model.addRow(new Object[]{nos.get(r), order.get(r), r.getName(), r.getSurname(), t1, t2, formatDuration(timesDiff.get(r),code.get(r),r), DNS_STRING, DNF_STRING, DSQ_STRING, code.get(r)});
        }
    }

    @Override
    public void refresh(){
        boolean w = true;
        for (Runner r: runners
        ) {
            if(code.get(r) < FINISHED_CODE){
                w = false;
            }
        }
        bCalculate.setEnabled(w);
    }

    static class ButtonRenderer extends JButton implements TableCellRenderer {

        public ButtonRenderer() {
            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            if (isSelected) {
                setForeground(table.getSelectionForeground());
                setBackground(table.getSelectionBackground());
            } else {
                setForeground(table.getForeground());
                setBackground(UIManager.getColor("Button.background"));
            }
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }

    class ButtonEditor extends DefaultCellEditor {

        protected JButton button;
        private String label;
        private boolean isPushed;

        public ButtonEditor(JCheckBox checkBox) {
            super(checkBox);
            button = new JButton();
            button.setOpaque(true);
            button.addActionListener(e -> {
                fireEditingStopped();
//                System.out.println(e.getActionCommand() + " : " + table.getSelectedRow());
                switch (label) {
                    case DNS_STRING -> code.put(runners.get(table.getSelectedRow()), DNS_CODE);
                    case DNF_STRING -> code.put(runners.get(table.getSelectedRow()), DNF_CODE);
                    case DSQ_STRING -> code.put(runners.get(table.getSelectedRow()), DSQ_CODE);
                }
                updateTable();
                refresh();
            });
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            if (isSelected) {
                button.setForeground(table.getSelectionForeground());
                button.setBackground(table.getSelectionBackground());
            } else {
                button.setForeground(table.getForeground());
                button.setBackground(table.getBackground());
            }
            label = (value == null) ? "" : value.toString();
            button.setText(label);
            isPushed = true;
            return button;
        }

        @Override
        public Object getCellEditorValue() {
            if (isPushed) {
                JOptionPane.showMessageDialog(button, label + ": Ouch!");
            }
            isPushed = false;
            return label;
        }

        @Override
        public boolean stopCellEditing() {
            isPushed = false;
            return super.stopCellEditing();
        }
    }


}
