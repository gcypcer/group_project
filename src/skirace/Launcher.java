//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

import javax.swing.*;

public class Launcher {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(AppController::new);
    }
}
