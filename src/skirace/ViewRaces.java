package skirace;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewRaces {
    private JFrame frame;
    private List<Race> races;
    private String[] raceNames;
    private Race currentRace;
    private final DefaultTableModel model;
    private JLabel organizer2;
    private JLabel mainJudge2;
    public ViewRaces(){
        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setSize(850, 400);

        DataBaseControl db = new DataBaseControl();
        races = db.readRaces();
        raceNames = new String[races.size()];
        for (int i  = 0; i < races.size(); i++){
            raceNames[i] = races.get(i).getName();
        }

        // North
        JPanel pNorth = new JPanel(new GridLayout(3, 2, 15, 15));
        JComboBox<String> raceChoice = new JComboBox<>(raceNames);
        raceChoice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseRace(raceChoice.getSelectedIndex());
            }
        });
        pNorth.add(raceChoice, 0, 0);
        JLabel organizer1 = new JLabel("Organizer: ");
        organizer2 = new JLabel();
        JLabel mainJudge1 = new JLabel("Main Judge: ");
        mainJudge2 = new JLabel();
        pNorth.add(new JPanel());
        pNorth.add(organizer1);
        pNorth.add(organizer2);
        pNorth.add(mainJudge1);
        pNorth.add(mainJudge2);

        pNorth.setBorder(new EmptyBorder(10, 10, 10, 10));
        frame.add(pNorth, BorderLayout.NORTH);

        // Center
        JPanel pCenter = new JPanel();
        model = new DefaultTableModel();
        JTable table = new JTable(model);
        table.setCellSelectionEnabled(false);

        String[] columnNames = {"Name", "Surname", "Start", "Finish", "Score"};
        for(String col : columnNames){
            model.addColumn(col);
        }

        table.setPreferredScrollableViewportSize(new Dimension(800, 200));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        pCenter.add(scrollPane);

        frame.add(pCenter, BorderLayout.CENTER);
    }

    public void show(){
        frame.setVisible(true);
    }

    private void updateTable(){
        model.setRowCount(0);
        for(Runner r : currentRace.getRunners()){
            LocalTime t1 = currentRace.getTimes1().get(r);
            if(t1 != null) {t1 = t1.truncatedTo(ChronoUnit.MILLIS);}
            LocalTime t2 = currentRace.getTimes2().get(r);
            if(t2 != null) {t2 = t2.truncatedTo(ChronoUnit.MILLIS);}
            model.addRow(new Object[] {r.getName(), r.getSurname(), t1, t2, currentRace.getScores().get(r)});
        }
    }

    private void chooseRace(int index){
        currentRace = races.get(index);
        updateTable();
        organizer2.setText(currentRace.getOrganizer());
        mainJudge2.setText(currentRace.getMainJudge());
    }
}
