//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł
package skirace;

import java.sql.*;
import java.time.LocalTime;
import java.util.*;
import java.time.LocalDate;

public class DataBaseControl {
//    private static final String DB_URL = "jdbc:h2:./src/skirace/SkiraceDB";
//    private static final String USER = "";
//    private static final String PASS = "";
//    private static final String host="ora4.ii.pw.edu.pl";
    private static final String USER="z34";
    private static final String PASS="x3vpzy";
    private static final String DB_URL = "jdbc:oracle:thin:@ora4.ii.pw.edu.pl:1521/pdb1.ii.pw.edu.pl";
    private Connection conn;
    private Statement stmt;

    public void testCon(){
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connection established");
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            _close();
        }
    }
    public void writeRunners(List<Runner> runners){
        try{
            ArrayList<Runner> existing_runners = readRunners();
            Race uniqueCheck = new Race("uniqueCheck", "uniqueCheck", "uniqueCheck");
            uniqueCheck.addRunners(existing_runners);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement ppst;
            for(Runner runner : runners){
                if (uniqueCheck.checkUniqueId(runner.getId())) {
                    ppst = conn.prepareStatement("INSERT INTO Runners VALUES (?, ?, ?, ?, ?, ?)");
                    ppst.setString(1, runner.getId());
                    ppst.setString(2, runner.getName());
                    ppst.setString(3, runner.getSurname());
                    ppst.setString(4, runner.getBirthYear());
                    ppst.setString(5, runner.getCategory());
                    ppst.setString(6, runner.getClub());
                    ppst.executeUpdate();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            _close();
        }
    }

    public ArrayList<Runner> readRunners(){
        ArrayList<Runner> runners = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Runners");
            while(rs.next()){
                Runner runner = new Runner(rs.getString("id"));
                runner.setName(rs.getString("name"));
                runner.setSurname(rs.getString("surname"));
                runner.setBirthYear(rs.getString("birthyear"));
                runner.setCategory(rs.getString("category"));
                runner.setClub(rs.getString("club"));
                runners.add(runner);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        _close();
        return runners;
    }
    public void writeRaces(List<Race> races){
        try{
            ArrayList<String> existing_names = readRaceNames();
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            PreparedStatement ppst;
            Timestamp timestamp1, timestamp2;
            LocalDate localDate = LocalDate.now();
            for(Race race: races){
                String name = race.getName();
                if (existing_names.contains(name)) {
                    continue;
                }
                ppst = conn.prepareStatement("INSERT INTO Races VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                ppst.setString(1, race.getOrganizer());
                ppst.setString(2, race.getMainJudge());
                ppst.setString(3, name);
                ppst.setString(4, race.COMPETITION_DATE);
                ppst.setInt(5, race.NUMBER_OF_ROUNDS);
                ppst.setInt(6, race.FORLAUFERS_COUNT);
                ppst.setInt(7, race.STARTING_INTERVAL);
                ppst.setInt(8, race.MIN_RESULT);
                ppst.setInt(9, race.MAX_RESULT);
                ppst.setInt(10, race.SCORE_ALGORITHM_TYPE);
                ppst.setInt(11, boolToInt(race.DIVIDE_BY_CATEGORY_AND_SEX));
                ppst.setInt(12, boolToInt(race.DISPLAY_SORTED_BY_SCORE));
                ppst.executeUpdate();
                for(Runner runner : race.getRunners()){
                    ResultSet rs1 = stmt.executeQuery("SELECT seq_runnerscon.nextval FROM Runnerscon");
                    ppst = conn.prepareStatement("INSERT INTO Runnerscon VALUES (?, ?, ?, ?, ?, ?, ?)");
                    rs1.next();
                    ppst.setInt(1, rs1.getInt(1));
                    ppst.setString(2, runner.getId());
                    ppst.setString(3, name);
//                    timestamp1 = Timestamp.valueOf(String.valueOf(race.times1.get(runner)));
                    timestamp1 = Timestamp.valueOf(race.times1.get(runner).atDate(localDate));
                    ppst.setTimestamp(4, timestamp1);
//                    timestamp2 = Timestamp.valueOf(String.valueOf(race.times2.get(runner)));
                    timestamp2 = Timestamp.valueOf(race.times2.get(runner).atDate(localDate));
                    ppst.setTimestamp(5, timestamp2);
                    ppst.setInt(6, race.scores.get(runner));
                    ppst.setInt(7, race.codes.get(runner));
                    ppst.executeUpdate();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            _close();
        }
    }

    private int boolToInt(boolean b) {
        return (b) ? 1 : 0;
    }

    private boolean intToBool(int i) {
        if (i == 0){return false;}
        return true;
    }

    public ArrayList<String> readRaceNames(){
        ArrayList<String> names = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            ResultSet rs1 = stmt.executeQuery("SELECT name FROM Races");
            while(rs1.next()){
                names.add(rs1.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            _close();
        }
        return names;
    }

    public ArrayList<Race> readRaces(){
        ArrayList<Race> races = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            ResultSet rs1 = stmt.executeQuery("SELECT * FROM Races");
            while(rs1.next()){
                Race race = new Race(rs1.getString("organizer"), rs1.getString("judge"), rs1.getString("name"), rs1.getString("competition_date"),
                        rs1.getInt("number_of_rounds"), rs1.getInt("forlaufers_count"), rs1.getInt("starting_interval"), rs1.getInt("min_res"),
                        rs1.getInt("max_res"), rs1.getInt("algorythm"), intToBool(rs1.getInt("divide")), intToBool(rs1.getInt("sorted")));
                races.add(race);
            }
            ResultSet rs2 = stmt.executeQuery("SELECT * FROM Runnerscon");
            String runnerid, racename;
            LocalTime t1, t2;
            int score, code;
            while(rs2.next()){
                runnerid = rs2.getString("runnerid");
                racename = rs2.getString("racename");
                t1 = rs2.getTimestamp("start_time").toLocalDateTime().toLocalTime();
                t2 = rs2.getTimestamp("finish_time").toLocalDateTime().toLocalTime();
                score = rs2.getInt("score");
                code = rs2.getInt("code");
                for(Race race: races) {
                    if(race.getName().equals(racename)){
                        for(Runner runner : this.readRunners()){
                            if(Objects.equals(runner.getId(), runnerid)){
                                race.addRunner(runner);
                                race.times1.put(runner, t1);
                                race.times2.put(runner, t2);
                                race.codes.put(runner, code);
                                race.scores.put(runner, score);
                                break;
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            _close();
        }
        return races;
    }

    public void _create() {
        // open a connection
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            stmt.execute("CREATE TABLE Races (organizer VARCHAR(50) NOT NULL, judge VARCHAR(30) NOT NULL,"+
                    "name VARCHAR(30) NOT NULL PRIMARY KEY, competition_date VARCHAR(10), number_of_rounds INT, forlaufers_count INT, " +
                    "starting_interval INT, min_res INT, max_res INT, algorythm INT, divide INT, sorted INT)");
            stmt.execute("CREATE TABLE Runners (id INT NOT NULL PRIMARY KEY, name VARCHAR(30) NOT NULL, surname VARCHAR(30) NOT NULL, "+
            "birthyear VARCHAR(4) NOT NULL, category VARCHAR(30), club VARCHAR(30))");
            stmt.execute("CREATE TABLE Runnerscon (id INT NOT NULL PRIMARY KEY, runnerid VARCHAR(11) NOT NULL, " +
                    "racename VARCHAR(30) NOT NULL, start_time TIMESTAMP(0), finish_time TIMESTAMP(0), score INT NOT NULL, code INT NOT NULL)");
            System.out.println("Database created successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            _close();
        }
    }

    private void _dropAll() {
        // open a connection
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            stmt.execute("DROP TABLE Runnerscon");
            stmt.execute("DROP TABLE Races");
            stmt.execute("DROP TABLE Runners");
            System.out.println("Database dropped successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            _close();
        }
    }

    private void _cleanAll(){
        _dropAll();
        _create();
    }

    public void insertTest() {
        // open a connection
        try{
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement ppst = null;
            ppst = conn.prepareStatement("INSERT INTO Runners VALUES (3, ?, ?, 2002, 0, 0)");
//            ppst = conn.prepareStatement("INSERT INTO Runnerscon (runnerid, racename) VALUES (?, ?)");
            ppst.setString(1, "Franek");
            ppst.setString(2, "Koziol");
            ppst.executeUpdate();
            stmt = conn.createStatement();

            System.out.println("Added succesfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            _close();
        }
    }

    private void addTestRunners(int size){
        List<Runner> runners = new ArrayList<>();
        Runner test;
        List<String> names = new ArrayList<>(Arrays.asList("Jan", "Paweł", "Agnieszka", "Zofia", "Marta", "Franciszek", "Grzegorz", "Adolf", "Józef", "Bożydar", "Magdalena"));
        List<String> surnames = new ArrayList<>(Arrays.asList("Kozioł", "Sosnowski", "Cypcer", "Stalin", "Kaczyński", "Kowalski", "Macierewicz", "Sasin", "Chopin", "Nowak", "Woźniak"));
        Random rand = new Random();
        for(int id = 1000; id < 1000 + size; id++){
            test = new Runner(String.valueOf(id), names.get(rand.nextInt(names.size())), surnames.get(rand.nextInt(surnames.size())), String.valueOf(1980 + rand.nextInt(25)));
            runners.add(test);
        }
        writeRunners(runners);
    }

    private void _close() {
        // close a connection
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main (String[]args){
        DataBaseControl db = new DataBaseControl();
//        db.testCon();
//        db._dropAll();
//        db._create();
//        db.insertTest();
//        db._cleanAll();
//        db._create();
//        db.addTestRunners(20);

    }

}
