//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Panel for phase 3
 */
public class ScorePanel extends Panel{
    private final DefaultTableModel model;
    public ScorePanel(JButton bPrev){
        timer = new Timer(1000, e -> updateTable());
        timer.start();

        this.bPrev = bPrev;

        Image backgroundImage = new ImageIcon("resources/score_background.png").getImage();
        image = backgroundImage;


        // South
        JPanel pSouth = new JPanel();
        pSouth.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 10));
        pSouth.setBorder(new EmptyBorder(0, 10, 10, 10));
        pSouth.add(this.bPrev);
        JButton fakeNext = new JButton("Next");
        pSouth.add(fakeNext);
        fakeNext.setEnabled(false);
        pSouth.setOpaque(false);
        panel.add(pSouth, BorderLayout.SOUTH);

        // Center
        JPanel pCenter = new JPanel();
        model = new DefaultTableModel();
        JTable table = new JTable(model);
        JTableHeader header = table.getTableHeader();
        header.setBackground(Color.CYAN);
        header.setForeground(Color.BLACK);



        String[] columnNames = {"No.", "Runner no.", "Name", "Surname", "Time", "Score"};
        for(String col : columnNames){
            model.addColumn(col);
        }

        updateTable();

        table.setPreferredScrollableViewportSize(new Dimension(800, 260));
        table.setFillsViewportHeight(false);
        table.setOpaque(false);
        JScrollPane scrollPane = new JScrollPane(table);
        //scrollPane.setOpaque(false);/
        pCenter.add(scrollPane);
        pCenter.setOpaque(false);
        panel.add(pCenter, BorderLayout.CENTER);
    }


    @Override
    public void updateTable(){
        if(races != null && !races.isEmpty() && races.get(0).DISPLAY_SORTED_BY_SCORE) {
            Collections.sort(runners, Comparator.comparing(o -> scores.get(o)));
        }
        model.setRowCount(0);
        for(Runner r : runners){
            LocalTime t1 = times1.get(r);
            if(t1 != null) {t1 = t1.truncatedTo(ChronoUnit.MILLIS);}
            LocalTime t2 = times2.get(r);
            if(t2 != null) {t2 = t2.truncatedTo(ChronoUnit.MILLIS);}
            model.addRow(new Object[] {nos.get(r), order.get(r), r.getName(), r.getSurname(), formatDuration(timesDiff.get(r),code.get(r),r), scores.get(r)});
        }
    }
    @Override
    public void refresh(){

    }


}