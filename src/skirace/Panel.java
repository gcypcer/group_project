//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł

package skirace;

import javax.swing.*;
import java.awt.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


public abstract class Panel{
    protected final int READY_CODE = 0;
    protected final int RUNNING_CODE = 1;
    protected final int FINISHED_CODE = 2;
    protected final int DNS_CODE = 3;
    protected final int DNF_CODE = 4;
    protected final int DSQ_CODE = 5;

    protected final String DNS_STRING = "DNS";
    protected final String DNF_STRING = "DNF";
    protected final String DSQ_STRING = "DSQ";
    protected Image image;
    protected JPanel panel = new JPanel(new BorderLayout(10, 10)){

        @Override
        public void paintComponent(Graphics g){
            g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        }
    };
    protected ArrayList<Runner> runners = new ArrayList<>();
    protected ArrayList<Race> races = new ArrayList<>();
    protected HashMap<Runner, Integer> nos = new HashMap<>();
    protected HashMap<Runner, Integer> order = new HashMap<>();
    protected HashMap<Runner, LocalTime> times1 = new HashMap<>();
    protected HashMap<Runner, LocalTime> times2 = new HashMap<>();
    protected HashMap<Runner, Duration> timesDiff = new HashMap<>();
    protected Race currentRace;
    /** code
     * 0 = before start
     * 1 = has started hasnt finished
     * 2 = finished
     * 3 = dns
     * 4 = dnf
     * 5 = dsq
     */
    protected HashMap<Runner, Integer> code = new HashMap<>();
    protected HashMap<Runner, Integer> scores = new HashMap<>();

    protected JButton bPrev;
    protected JButton bNext;

    protected Timer timer;
    protected int roundNumber = 1;

    public ArrayList<Runner> getRunners(){
        return runners;
    }
    public ArrayList<Race> getRaces(){
        return races;
    }
    public HashMap<Runner, Integer> getNos(){
        return nos;
    }
    public HashMap<Runner, Integer> getOrder(){
        return order;
    }
    public HashMap<Runner, LocalTime> getTimes1(){
        return times1;
    }
    public HashMap<Runner, LocalTime> getTimes2(){
        return times2;
    }
    public HashMap<Runner, Duration> getTimesDiff(){
        return timesDiff;
    }
    public HashMap<Runner, Integer> getCode() {return code;}
    public HashMap<Runner, Integer> getScores(){
        return scores;
    }

    public void updateData(ArrayList<Race> races, ArrayList<Runner> runners, HashMap<Runner, Integer> nos, HashMap<Runner, Integer> order, HashMap<Runner, LocalTime> times1, HashMap<Runner, LocalTime> times2, HashMap<Runner, Duration> timesDiff,HashMap<Runner, Integer> code, HashMap<Runner, Integer> scores){
        this.races = races;
        this.runners = runners;
        this.nos = nos;
        this.order = order;
        this.times1 = times1;
        this.times2 = times2;
        this.timesDiff = timesDiff;
        this.code = code;
        this.scores = scores;
    }

    abstract public void updateTable();

    abstract public void refresh();

    public String formatDuration(Duration duration, int c, Runner r) {
        if(duration == null || c != 2){
            if(c == 0){
                return "---";
            }
            else if(c == 1){
                duration = Duration.between(times1.get(r), LocalTime.now());
                long seconds = duration.getSeconds();
                long absSeconds = Math.abs(seconds);
                String positive = String.format(
                        "%02d:%02d",
                        (absSeconds % 3600) / 60,
                        absSeconds % 60);
                return seconds < 0 ? "-" + positive : positive;
            }
            else if(c == 3) {
                return "DNS";
            }
            else if(c == 4) {
                return "DNF";
            }
            else if(c == 5) {
                return "DSQ";
            }
        }
        assert duration != null;
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%02d:%02d:%03d",
                (absSeconds % 3600) / 60,
                absSeconds % 60,
                duration.toMillis() % 1000);

        return seconds < 0 ? "-" + positive : positive;
    }

    public Race getRace(String name){
        return currentRace;
    }


}
