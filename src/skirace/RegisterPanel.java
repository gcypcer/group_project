//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Panel for phase 1
 */
public class RegisterPanel extends Panel{

    private static int registerNumber = 0;
    private final JTable table;
    private final DefaultTableModel model;
    private int no = 1;

    private String getNextId(){

        SimpleDateFormat f = new SimpleDateFormat("ddMMyy");
        registerNumber++;
        String id = f.format(new Date()) + String.format("%05d", registerNumber);
        return id;
    }

    public RegisterPanel(JButton bPrev, JButton bNext) {
        this.bPrev = bPrev;
        this.bNext = bNext;

        Image backgroundImage = new ImageIcon("resources/register_background.png").getImage();
        image = backgroundImage;

        // East
        JPanel pEast = new JPanel(new GridLayout(0, 1 , 10, 35));
        pEast.setBorder(new EmptyBorder(25, 0, 25, 20));
        JButton bCreate = new JButton("Create");
        bCreate.setBackground(Color.cyan);
        bCreate.addActionListener(e -> addRunner());
        JButton bRead = new JButton("Read");
        bRead.setBackground(Color.cyan);
        bRead.addActionListener(e -> readRunner());
        JButton bUpdate = new JButton("Update");
        bUpdate.setBackground(Color.cyan);
        bUpdate.addActionListener(e -> updateRunner());
        JButton bDelete = new JButton("Delete");
        bDelete.setBackground(Color.red);
        bDelete.addActionListener(e -> deleteRunner());

        pEast.add(bCreate);
        pEast.add(bRead);
        pEast.add(bUpdate);
        pEast.add(bDelete);
        pEast.setOpaque(false);
        panel.add(pEast, BorderLayout.EAST);

        // South
        JPanel pSouth = new JPanel();
        pSouth.setLayout(new GridLayout(2, 0, 10, 10));
        pSouth.setBorder(new EmptyBorder(0, 10, 10, 10));

        JPanel pImportExport = new JPanel();
        pImportExport.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        JButton bImportExport = new JButton("import/export starting list");
        JButton bClearData = new JButton("clear last times");
        JButton bDrawOrder = new JButton("draw order");
        JButton bDrawNumbers = new JButton("draw numbers");

        bImportExport.addActionListener(e -> ImportExport());
        bDrawOrder.addActionListener(e -> drawOrder());
        bDrawNumbers.addActionListener(e -> drawNumbers());
        bClearData.addActionListener(e -> clearData());

        pImportExport.add(bImportExport);
        pImportExport.add(bClearData);
        pImportExport.add(bDrawOrder);
        pImportExport.add(bDrawNumbers);

        pImportExport.setOpaque(false);

        pSouth.add(pImportExport);

        JPanel pPrevNext = new JPanel();
        pPrevNext.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 10));
        pPrevNext.add(this.bPrev);
        pPrevNext.add(this.bNext);
        this.bNext.setEnabled(false);
        pPrevNext.setOpaque(false);

        pSouth.add(pPrevNext);

        pSouth.setOpaque(false);

        panel.add(pSouth, BorderLayout.SOUTH);

        // Center
        JPanel pCenter = new JPanel();
        pCenter.setBorder(new EmptyBorder(10, 10, 10, 10));

        model = new DefaultTableModel();
        table = new JTable(model);

        String[] columnNames = {"No.", "Runner no.", "Name", "Surname"};
        for(String col : columnNames){
            model.addColumn(col);
        }

        updateTable();

        table.setPreferredScrollableViewportSize(new Dimension(700, 200));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        pCenter.add(scrollPane);

        panel.add(pCenter, BorderLayout.CENTER);
    }

    public void clearData(){
        order = new HashMap<>();
        times1 = new HashMap<>();
        times2 = new HashMap<>();
        timesDiff = new HashMap<>();
        updateTable();
    }

    public void drawOrder(){
        Collections.shuffle(runners.subList(0,races.get(0).FORLAUFERS_COUNT));
        Collections.shuffle(runners.subList(races.get(0).FORLAUFERS_COUNT, runners.size()));
        updateTable();
    }

    public void drawNumbers(){
        int runnerNo = 1;
        int forlauferNo = -races.get(0).FORLAUFERS_COUNT;
        for(Runner r : runners){
            if(r.getCategory().equals("forlaufer")){
                order.put(r, forlauferNo++);
                code.put(r,0);
            }
            else{
                order.put(r, runnerNo++);
                code.put(r,0);
            }
        }
        updateTable();
        this.bNext.setEnabled(true);
    }

    public void ImportExport(){
        DataBaseControl db = new DataBaseControl();
        List<Runner> db_runners = db.readRunners();
        for (Runner runner : db_runners){
            runners.add(runner);
            nos.put(runner, no++);
            this.bNext.setEnabled(false);
            updateTable();
        }
    }

    private void addRunner(){
        final String[] labels = {"Name: ", "Surname: ", "Birth year: ", "*Category: ", "*Club: "};
        final JTextField[] textField = new JTextField[labels.length];
        //Create and populate the panel.
        JPanel p = new JPanel(new SpringLayout());
        for (int i = 0; i < labels.length; i++) {
            JLabel l = new JLabel(labels[i], JLabel.TRAILING);
            p.add(l);
            textField[i] = new JTextField(10);
            l.setLabelFor(textField[i]);
            p.add(textField[i]);
        }
        JButton button = new JButton("Submit");
        JLabel label = new JLabel("* is optional");
        label.setFont(new Font("Serif", Font.PLAIN, 10));
        p.add(label);
        p.add(button);
        //p.add()

        //Lay out the panel.
        SpringUtilities.makeCompactGrid(p,
                labels.length + 1, 2, //rows, cols
                7, 7,        //initX, initY
                7, 7);       //xPad, yPad


        //Create and set up the window.
        JFrame runnerFrame = new JFrame("Add runner");
        runnerFrame.setLocationRelativeTo(null);
        runnerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        runnerFrame.setResizable(false);

        button.addActionListener(e -> {
            String id, name, surname, birthYear, category, club;
            boolean isOk = true;
            id = getNextId();
            name = textField[0].getText();
            surname = textField[1].getText();
            birthYear = textField[2].getText();
            if(id.isEmpty() || name.isEmpty() || surname.isEmpty() || birthYear.isEmpty())
            {
                isOk = false;
                JOptionPane.showMessageDialog(runnerFrame, "Missing required data!",
                        "Add runner", JOptionPane.ERROR_MESSAGE);
            }
            else if(birthYear.length() != 4){
                isOk = false;
                JOptionPane.showMessageDialog(runnerFrame, "Birth year has to be 4 characters long!",
                        "Add runner", JOptionPane.ERROR_MESSAGE);
            }
            category = textField[3].getText();
            club = textField[4].getText();

            if(isOk){
                Runner runner = new Runner(id, name, surname, birthYear, category, club);
                JOptionPane.showMessageDialog(runnerFrame, "New runner added.", "Add runner", JOptionPane.PLAIN_MESSAGE);
                if(runner.getCategory().equals("forlaufer")){
                    Collections.reverse(runners);
                    runners.add(runner);
                    Collections.reverse(runners);
                    nos.put(runner, no++);
                }
                else {
                    runners.add(runner);
                    nos.put(runner, no++);
                }
                this.bNext.setEnabled(false);
                updateTable();
            }

            //clear textFields
            for(JTextField tF : textField){
                tF.setText("");
            }
        });

        //Set up the content pane.
        p.setOpaque(true);  //content panes must be opaque
        runnerFrame.setContentPane(p);

        //Display the window.
        runnerFrame.pack();
        runnerFrame.setVisible(true);
    }

    private void deleteRunner(){
        Runner r = getSelectedRunner();
        if(r == null){
            return;
        }
        int n = JOptionPane.showConfirmDialog(panel, "Are you sure?");
        if(n == JOptionPane.YES_OPTION){
            nos.remove(r);
            runners.remove(r);
            updateTable();
        }
    }

    private void readRunner(){
        Runner runner = getSelectedRunner();
        if (runner == null){
            return;
        }
        JFrame fRead = new JFrame("Runner " + runner.getId());
        fRead.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        fRead.setSize(250, 300);
        fRead.setResizable(false);

        JPanel pEast = new JPanel(new GridLayout(0, 1, 5, 10));
        pEast.setBorder(new EmptyBorder(10, 10, 10, 10));
        pEast.add(new JLabel(runner.getId()));
        pEast.add(new JLabel(runner.getName()));
        pEast.add(new JLabel(runner.getSurname()));
        pEast.add(new JLabel(runner.getBirthYear()));
        pEast.add(new JLabel(runner.getCategory()));
        pEast.add(new JLabel(runner.getClub()));
        fRead.add(pEast, BorderLayout.EAST);

        JPanel pWest = getLabelsPanel();
        fRead.add(pWest, BorderLayout.WEST);

        fRead.setLocationRelativeTo(panel);
        fRead.setVisible(true);
    }

    private void updateRunner(){
        Runner runner = getSelectedRunner();
        if (runner == null){
            return;
        }
        JFrame fRead = new JFrame("Runner " + runner.getId());
        fRead.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        fRead.setSize(350, 300);
        fRead.setResizable(false);

        JPanel pEast = new JPanel(new GridLayout(0, 1, 5, 10));
        pEast.setBorder(new EmptyBorder(10, 10, 10, 10));
        JTextField tfName = new JTextField(runner.getName());
        JTextField tfSurname = new JTextField(runner.getSurname());
        JTextField tfYear = new JTextField(runner.getBirthYear());
        JTextField tfCategory = new JTextField(runner.getCategory());
        JTextField tfClub = new JTextField(runner.getClub());
        pEast.add(tfName);
        pEast.add(tfSurname);
        pEast.add(tfYear);
        pEast.add(tfCategory);
        pEast.add(tfClub);
        fRead.add(pEast, BorderLayout.CENTER);

        JPanel pCenter = getLabelsPanel();
        pCenter.remove(0);
        fRead.add(pCenter, BorderLayout.WEST);

        JPanel pSouth = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton bUpdate = new JButton("Update");
        bUpdate.addActionListener(e -> {
            int n = JOptionPane.showConfirmDialog(panel, "Are you sure?");
            if(n == JOptionPane.YES_OPTION){
                runner.setName(tfName.getText());
                runner.setSurname(tfSurname.getText());
                runner.setBirthYear(tfYear.getText());
                runner.setCategory(tfCategory.getText());
                runner.setClub(tfClub.getText());
                updateTable();
                fRead.dispose();
            }
        });
        pSouth.add(bUpdate);
        pSouth.setBorder(new EmptyBorder(0, 10, 10, 10));
        fRead.add(pSouth, BorderLayout.SOUTH);

        fRead.setLocationRelativeTo(panel);
        fRead.setVisible(true);
    }

    private JPanel getLabelsPanel(){
        JPanel pWest = new JPanel(new GridLayout(0, 1, 5, 10));
        pWest.setBorder(new EmptyBorder(10, 10, 10, 10));
        pWest.add(new JLabel("Id: "));
        pWest.add(new JLabel("Name: "));
        pWest.add(new JLabel("Surname: "));
        pWest.add(new JLabel("Year of birth: "));
        pWest.add(new JLabel("Category: "));
        pWest.add(new JLabel("Club: "));
        return pWest;
    }

    public Runner getSelectedRunner(){
        int index = table.getSelectedRow();
        if(index != -1) {
            return runners.get(table.getSelectedRow());
        }
        return null;
    }

    @Override
    public void updateTable() {
        no = 1;
        model.setRowCount(0);
        for(Runner r : runners){
            nos.put(r,no++);
            model.addRow(new Object[]{nos.get(r), order.get(r), r.getName(), r.getSurname()});
        }
    }

    @Override
    public void refresh(){

    }
}
