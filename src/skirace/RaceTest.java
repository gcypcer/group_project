//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł

package skirace;

import static org.junit.jupiter.api.Assertions.*;

class RaceTest {

    Race race = new Race("WKN", "Jan Sosnowski", "Warsaw Ski Cup");

    @org.junit.jupiter.api.Test
    void addRunner() {
        int prev = race.getRunnersCount();
        race.addRunner(new Runner("02122200001","Jan","Sosnowski","2001"));
        assertEquals(race.getRunnersCount(), (prev + 1));
    }

    @org.junit.jupiter.api.Test
    void getRunnersCount() {
        int prev = race.getRunnersCount();
        race.addRunner(new Runner("02122200002","Jan","Sosnowski","2002"));
        race.addRunner(new Runner("02122200002","Jan","Sosnowski","2002"));
        assertEquals(race.getRunnersCount(), prev + 1);
    }
}