//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


//main

package skirace;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static java.lang.String.format;
import static java.util.Arrays.asList;

public class WelcomePanel extends Panel{

    //competition options
    private String COMPETITION_NAME = "skirace5";
    private String ORGANIZER = "PAP";
    private String COMPETITION_DATE = LocalDate.now().toString();
    private String MAIN_JUDGE = "MHWESIUK";
    private int NUMBER_OF_ROUNDS = 1;

    //starting options
    private int FORLAUFERS_COUNT = 0;

    //race options
    private int STARTING_INTERVAL = 0;
    private int MIN_RESULT = 0;
    private int MAX_RESULT = Integer.MAX_VALUE;

    //result options
    private int SCORE_ALGORITHM_TYPE = 0;
    private boolean DIVIDE_BY_CATEGORY_AND_SEX = true; //false means open category
    private boolean DISPLAY_SORTED_BY_SCORE = true; //false means by place AND by category


    public WelcomePanel(JButton bNext) {
//        Runner exampleRunner = new Runner("8931672", "Franek", "koziol", "2022");
//        int no = 1;
//        for(int i =0; i<=5; i++){
//            runners.add(exampleRunner);
//            nos.put(exampleRunner, no++);
//            exampleRunner = new Runner("8931672", "Franek", "koziol", "2022");
//        }
//        DataBaseControl db = new DataBaseControl();
//        ArrayList<Runner> nrunners = db.readRunners();
//        Runner exampleRunner;
//        int no = 1;
//        for(int i =0; i<3; i++) {
//            exampleRunner = nrunners.get(i);
//            runners.add(exampleRunner);
//            nos.put(exampleRunner, no++);
//        }

        ViewRaces viewFrame = new ViewRaces();

        this.bNext = bNext;

        JButton bSave = new JButton("Save configuration");

        Image backgroundImage = new ImageIcon("resources/bg.jpeg").getImage();
        image = backgroundImage;


        //menus


        JMenuBar mb = new JMenuBar();
        JMenuItem menuItem;
        JMenu menu1 = new JMenu("competition options");
        JTextField input = new JTextField("competition name");
        input.addPropertyChangeListener(e -> {
            COMPETITION_NAME = ((JTextField) e.getSource()).getText();
            bSave.setEnabled(true);
        });
        menu1.add(input);
        input = new JTextField("organizer name");
        input.addPropertyChangeListener(e -> {
            ORGANIZER = ((JTextField) e.getSource()).getText();
            bSave.setEnabled(true);
        });
        menu1.add(input);
        input = new JTextField(LocalDate.now().toString());
        input.addPropertyChangeListener(e -> {
            COMPETITION_DATE = ((JTextField) e.getSource()).getText();
            bSave.setEnabled(true);
        });
        menu1.add(input);
        input = new JTextField("number of rounds");
        input.addPropertyChangeListener(e -> {
            try {
                NUMBER_OF_ROUNDS = Integer.parseInt(((JTextField) e.getSource()).getText());
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }
        });
        menu1.add(input);
        input = new JTextField("main judge");
        input.addPropertyChangeListener(e -> {
            try {
                MAIN_JUDGE = ((JTextField) e.getSource()).getText();
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }
        });
        menu1.add(input);

        mb.add(menu1);

        JMenu menu2 = new JMenu("starting options");
        input = new JTextField("forlaufers count");
        input.addPropertyChangeListener(e -> {
            try {
                FORLAUFERS_COUNT = Integer.parseInt(((JTextField) e.getSource()).getText());
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }
        });
        menu2.add(input);

        mb.add(menu2);

        JMenu menu3 = new JMenu("race options");
        input = new JTextField("starting interval");
        input.addPropertyChangeListener(e -> {
            try {
                STARTING_INTERVAL = Integer.parseInt(((JTextField) e.getSource()).getText());
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }
        });
        menu3.add(input);
        input = new JTextField("min time");
        input.addPropertyChangeListener(e -> {
            try {
                MIN_RESULT = Integer.parseInt(((JTextField) e.getSource()).getText());
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }
        });
        menu3.add(input);
        input = new JTextField("max time");
        input.addPropertyChangeListener(e -> {
            try {
                MAX_RESULT = Integer.parseInt(((JTextField) e.getSource()).getText());
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }

        });
        menu3.add(input);

        mb.add(menu3);

        JMenu menu4 = new JMenu("result options");
        input = new JTextField("score algorithm type");
        input.addPropertyChangeListener(e -> {
            try {
                SCORE_ALGORITHM_TYPE = Integer.parseInt(((JTextField) e.getSource()).getText());
                bSave.setEnabled(true);
            } catch (NumberFormatException ex) {
                //ex.printStackTrace();
            }
        });
        menu4.add(input);
        JCheckBoxMenuItem inputCheck = new JCheckBoxMenuItem("divide by sex and category");
        inputCheck.setSelected(true);
        inputCheck.addItemListener(e -> {
            DIVIDE_BY_CATEGORY_AND_SEX = e.getStateChange()==1;
            bSave.setEnabled(true);
        });
        menu4.add(inputCheck);
        inputCheck = new JCheckBoxMenuItem("sort by score");
        inputCheck.setSelected(true);
        inputCheck.addItemListener(e -> {
            DISPLAY_SORTED_BY_SCORE = e.getStateChange()==1;
            bSave.setEnabled(true);
        });
        menu4.add(inputCheck);

        mb.add(menu4);

        // North
        JPanel pNorth = new JPanel();
        pNorth.add(mb);
        panel.add(pNorth, BorderLayout.NORTH);

        // West
        JPanel pWest = new JPanel();
        //pWest.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        pWest.setLayout(new GridLayout(0, 1, 10, 10));
        pWest.setBorder(new EmptyBorder(10, 20, 10, 10));
        pWest.setBorder(new EmptyBorder(10, 20, 10, 10));
        JLabel lWelcome = new JLabel("Welcome, choose your settings");
        lWelcome.setFont(new Font("SansSerif", Font.BOLD, 22));
        pWest.add(lWelcome);

        JTextArea textArea = new JTextArea("Choose your settings above or load below from json file\n your configuration will be saved in <comeptiton name><date>.json file for future usages");
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setOpaque(false);
        textArea.setFont((new Font("SansSerif", Font.BOLD, 12)));
        pWest.add(textArea);
        /*pWest.setLayout(new GridLayout(0, 1, 10, 10));
        pWest.setBorder(new EmptyBorder(10, 20, 10, 10));
        JButton bCompOptions = new JButton("Select competition options");
\        JButton bStartingOptions = new JButton("Select starting options");
        JButton bRaceOptions = new JButton("Select race options");
        JButton bPunctOptions = new JButton("Select punctuation options");


        pWest.add(bCompOptions);
        pWest.add(bStartingOptions);
        pWest.add(bRaceOptions);
        pWest.add(bPunctOptions);*/
        pWest.setBackground(new Color(168,168,168, 174));
        panel.add(pWest, BorderLayout.WEST);


        // South
        JPanel pSouth = new JPanel();
        pSouth.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 10));
        pSouth.setBorder(new EmptyBorder(10, 10, 10, 10));
        JButton fakePrev = new JButton("Prev");
        fakePrev.setEnabled(false);
        bSave.addActionListener(e ->
        {
            saveConfigToFile(format("resources/%s%s.json", COMPETITION_NAME, COMPETITION_DATE));
            bSave.setEnabled(false);
        });
        JTextField inputConfig = new JTextField("default_config.json");
        inputConfig.addActionListener(e->{
            try {
                String path = "resources/" + ((JTextField) e.getSource()).getText();
                System.out.println(path);
                loadConfigFromFile(path);
                bSave.setEnabled(true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        JButton viewRaces = new JButton("view saved races");
        viewRaces.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewFrame.show();
            }
        });

        pSouth.add(viewRaces);
        pSouth.add(inputConfig);
        pSouth.add(bSave);
        pSouth.add(fakePrev);
        this.bNext.addActionListener(e -> updateRaces());
        pSouth.add(this.bNext);
        pSouth.setOpaque(false);
        panel.add(pSouth, BorderLayout.SOUTH);

        // Center
        JPanel pCenter = new JPanel();
        pCenter.setOpaque(false);
        panel.add(pCenter, BorderLayout.CENTER);



        panel.setVisible(true);

    }

    private void saveConfigToFile(String path){
        JSONArray o = new JSONArray();
        JSONObject co = new JSONObject();
        JSONObject so = new JSONObject();
        JSONObject rao = new JSONObject();
        JSONObject reo = new JSONObject();
        co.put("COMPETITION_NAME",COMPETITION_NAME);
        co.put("ORGANIZER",ORGANIZER);
        co.put("COMPETITION_DATE",COMPETITION_DATE);
        co.put("NUMBER_OF_ROUNDS",NUMBER_OF_ROUNDS);
        o.put(co);
        so.put("FORLAUFERS_COUNT",FORLAUFERS_COUNT);
        o.put(so);
        rao.put("STARTING_INTERVAL",STARTING_INTERVAL);
        rao.put("MIN_RESULT",MIN_RESULT);
        rao.put("MAX_RESULT",MAX_RESULT);
        o.put(rao);
        reo.put("SCORE_ALGORITHM_TYPE",SCORE_ALGORITHM_TYPE);
        reo.put("DIVIDE_BY_CATEGORY_AND_SEX",DIVIDE_BY_CATEGORY_AND_SEX);
        reo.put("DISLAY_SORTED_BY_SCORE",DISPLAY_SORTED_BY_SCORE);
        o.put(reo);

        //String path = "resources/active_config.json";
        try (PrintWriter out = new PrintWriter(new FileWriter(path))) {
            out.write(o.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void loadConfigFromFile(String path){
        try {
            JSONParser parser = new JSONParser();
            org.json.simple.JSONArray a = (org.json.simple.JSONArray) parser.parse(new FileReader(path));
            org.json.simple.JSONObject o = (org.json.simple.JSONObject) a.get(0);
            COMPETITION_NAME = (String) o.get("COMPETITION_NAME");
            ORGANIZER = (String) o.get("ORGANIZER");
            COMPETITION_DATE = (String) o.get("COMPETITION_DATE");
            NUMBER_OF_ROUNDS = (int)(long) o.get("NUMBER_OF_ROUNDS");
            o = (org.json.simple.JSONObject) a.get(1);
            FORLAUFERS_COUNT = (int)(long) o.get("FORLAUFERS_COUNT");
            o = (org.json.simple.JSONObject) a.get(2);
            STARTING_INTERVAL = (int)(long) o.get("STARTING_INTERVAL");
            MIN_RESULT = (int)(long) o.get("MIN_RESULT");
            MAX_RESULT = (int)(long) o.get("MAX_RESULT");
            o = (org.json.simple.JSONObject) a.get(3);
            SCORE_ALGORITHM_TYPE = (int)(long) o.get("SCORE_ALGORITHM_TYPE");
            DIVIDE_BY_CATEGORY_AND_SEX = (boolean) o.get("DIVIDE_BY_CATEGORY_AND_SEX");
            DISPLAY_SORTED_BY_SCORE = (boolean) o.get("DISPLAY_SORTED_BY_SCORE");


        } catch (IOException | ParseException e) {
            //e.printStackTrace();
        }
    }

    public void updateRaces(){
        boolean notexists = true;
        for (Race race : races){
            if (race.getName().equals(COMPETITION_NAME) && race.COMPETITION_DATE.equals(COMPETITION_DATE)){
                currentRace = race;
                notexists = false;
                break;}
        }
        if (notexists){
            Race race = new Race(ORGANIZER, MAIN_JUDGE, COMPETITION_NAME, COMPETITION_DATE, NUMBER_OF_ROUNDS, FORLAUFERS_COUNT, STARTING_INTERVAL, MIN_RESULT, MAX_RESULT, SCORE_ALGORITHM_TYPE, DIVIDE_BY_CATEGORY_AND_SEX, DISPLAY_SORTED_BY_SCORE);
            races.add(race);
            currentRace = race;
        }
    }

    @Override
    public void updateTable() {

    }

    @Override
    public void refresh(){

    }
}
