//created by Jan Sosnowski Grzegorz Cypcer Franciszek Kozioł


package skirace;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Race {
    private String organizer;
    private String mainJudge;
    private String name;
    private ArrayList<Runner> runners;
    public String COMPETITION_DATE;
    public int NUMBER_OF_ROUNDS;
    //starting options
    public int FORLAUFERS_COUNT;
    //race options
    public int STARTING_INTERVAL;
    public int MIN_RESULT;
    public int MAX_RESULT;
    //result options
    public int SCORE_ALGORITHM_TYPE;
    public boolean DIVIDE_BY_CATEGORY_AND_SEX; //false means open category
    public boolean DISPLAY_SORTED_BY_SCORE; //false means by place AND by category
    public HashMap<Runner, LocalTime> times1;
    public HashMap<Runner, LocalTime> times2;
    public HashMap<Runner, Integer> codes;
    public HashMap<Runner, Integer> scores;

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getMainJudge() {
        return mainJudge;
    }

    public void setMainJudge(String mainJudge) {
        this.mainJudge = mainJudge;
    }

    public HashMap<Runner, LocalTime> getTimes1(){return times1;}
    public void setTimes1(HashMap<Runner, LocalTime> times){this.times1 = times;}
    public HashMap<Runner, LocalTime> getTimes2(){return times2;}
    public void setTimes2(HashMap<Runner, LocalTime> times){this.times2 = times;}
    public HashMap<Runner, Integer> getCodes(){return codes;}
    public void setCodes(HashMap<Runner, Integer> codes){this.codes = codes;}
    public HashMap<Runner, Integer> getScores(){return scores;}
    public void setScores(HashMap<Runner, Integer> scores){this.scores = scores;}

    public Race(String organizer, String mainJudge, String name, String COMPETITION_DATE, int NUMBER_OF_ROUNDS, int FORLAUFERS_COUNT, int STARTING_INTERVAL, int MIN_RESULT, int MAX_RESULT, int SCORE_ALGORITHM_TYPE, boolean DIVIDE_BY_CATEGORY_AND_SEX, boolean DISPLAY_SORTED_BY_SCORE) {
        this.organizer = organizer;
        this.mainJudge = mainJudge;
        this.name = name;
        this.runners = new ArrayList<Runner>();
        this.COMPETITION_DATE = COMPETITION_DATE;
        this.NUMBER_OF_ROUNDS = NUMBER_OF_ROUNDS;
        this.FORLAUFERS_COUNT = FORLAUFERS_COUNT;
        this.STARTING_INTERVAL = STARTING_INTERVAL;
        this.MIN_RESULT = MIN_RESULT;
        this.MAX_RESULT = MAX_RESULT;
        this.SCORE_ALGORITHM_TYPE = SCORE_ALGORITHM_TYPE;
        this.DIVIDE_BY_CATEGORY_AND_SEX = DIVIDE_BY_CATEGORY_AND_SEX;
        this.DISPLAY_SORTED_BY_SCORE = DISPLAY_SORTED_BY_SCORE;
        this.times1 = new HashMap<>();
        this.times2 = new HashMap<>();
        this.scores = new HashMap<>();
        this.codes = new HashMap<>();

    }

    public Race(String organizer, String mainJudge, String name) {
        this.organizer = organizer;
        this.mainJudge = mainJudge;
        this.name = name;
        this.runners = new ArrayList<Runner>();
        this.COMPETITION_DATE = LocalDate.now().toString();
        this.NUMBER_OF_ROUNDS = 1;
        this.FORLAUFERS_COUNT = 0;
        this.STARTING_INTERVAL = 0;
        this.MIN_RESULT = 0;
        this.MAX_RESULT = Integer.MAX_VALUE;
        this.SCORE_ALGORITHM_TYPE = 0;
        this.DIVIDE_BY_CATEGORY_AND_SEX = true;
        this.DISPLAY_SORTED_BY_SCORE = true;
        this.times1 = new HashMap<>();
        this.times2 = new HashMap<>();
        this.scores = new HashMap<>();
        this.codes = new HashMap<>();
    }

    /**
     * looks through runners to check if
     * @param id is already taken
     * @return false if id is taken, true otherwise
     */
    public boolean checkUniqueId(String id){
        for(Runner runner : runners){
            if(runner.getId().equals(id)){
                return false;
            }
        }
        return true;
    }

    /**
     * try to add new
     * @param runner to runners list
     */
    public void addRunner(Runner runner){
        if(checkUniqueId(runner.getId())){
            runners.add(runner);
        }
    }

    public void addRunners(ArrayList<Runner> runners){
        for(Runner runner : runners){
            addRunner(runner);
        }
    }

    /**
     * simply
     * @return runners.size()
     */
    public int getRunnersCount(){
        return runners.size();
    }

    public ArrayList<Runner> getRunners(){
        return runners;
    }


    public static void main(String[] args) {
        Race race = new Race("WKN", "Jan Sosnowski", "Warsaw Ski Cup");
        race.addRunner(new Runner("02122200001","Jan","Sosnowski","2001"));
        race.addRunner(new Runner("02122200001","Jan","Sosnowski","2002"));
        race.addRunner(new Runner("02122200002","Franek","Koziol","2002"));
        System.out.println(race.getRunnersCount());
    }
}
